package net.lil.umblur

import android.app.PendingIntent
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.activity.result.contract.ActivityResultContracts.StartActivityForResult
import androidx.appcompat.app.AppCompatActivity
import com.tumblr.jumblr.JumblrClient
import net.openid.appauth.*
import org.scribe.model.Token

const val TAG = "MainAct"

class MainActivity : AppCompatActivity() {

    private lateinit var service: AuthorizationService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        service = AuthorizationService(this)

        doAuth() // async?
    }

    override fun onDestroy() {
        super.onDestroy()
        service.dispose()
    }

    private fun doAuth() {
        val config = AuthorizationServiceConfiguration(
            Uri.parse("https://www.tumblr.com/oauth/authorize"),
            Uri.parse("https://www.tumblr.com/oauth/access_token"),
            Uri.parse("https://www.tumblr.com/oauth/request_token")
        )

        val request = AuthorizationRequest.Builder(
            config,
            consumerKey,
            AuthorizationRequest.CODE_CHALLENGE_METHOD_S256,
            Uri.parse("umbler://dash.place")
        ).build()

        service.performAuthorizationRequest(
            request,
            PendingIntent.getActivity(
                applicationContext,
                0,
                Intent(this, MainActivity::class.java),
                PendingIntent.FLAG_IMMUTABLE
            )
        )

        launcher.launch(service.getAuthorizationRequestIntent(request))
    }

    private val launcher = registerForActivityResult(StartActivityForResult()) {
        Log.wtf("TEST", "OK")
        if (it.resultCode == RESULT_OK) {
            val authResponse = AuthorizationResponse.fromIntent(it.data!!)
            val authException = AuthorizationException.fromIntent(it.data)

            if (authResponse != null) {
                Log.i("Auth", "succ")

                service.performTokenRequest(
                    authResponse.createTokenExchangeRequest()
                ) { tokenResponse, tokenException ->
                    if (tokenResponse != null) {
                        Log.i("Token", "succ")

                        // Create a new client
                        val client = JumblrClient(consumerKey, consumerSecret)

                        Log.d(
                            TAG,
                            "$tokenResponse, ${tokenResponse.accessToken}, ${tokenResponse.idToken}"
                        )

                        //client.setToken() todo

                        // Write the user's name
                        val user = client.user()
                        Log.i(TAG, user.name)

                        // And list their blogs
                        for (blog in user.blogs) {
                            Log.i(TAG, "\t" + blog.title)
                        }

                        //val posts = client.userDashboard()

                    } else {
                        // authorization failed
                        Log.e("Token", tokenException.toString())
                    }
                }
            } else {
                // authorization failed
                Log.e("Auth", authException.toString())
            }
        }
    }
}
